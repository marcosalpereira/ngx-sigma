# ngx-sigma

Leia em [português](README.md).

Angular library using [sigmajs] for rendering directed graphs.

## Inside the box

- Nodes with images
- Label on edges
- Drill down. Loading navigation by double clicking on nodes.
- Auto loading all navigable nodes
- Dynamic layout (using ForceAtlas2 plugin)
- Save/Restore graph state
- Search for node and edges for any property existing in your data. And apply actions like::
    - Mark: Highlite nodes witha dynamic color
    - Mark descendings: Highlite nodes and yous descending with a dynamic color
    - Hide: Hide a node and your descending
    - Collapse: Collapse a node and your descending

## Example application

https://marcosalpereira.gitlab.io/ngx-sigma/

This application loads the brasilian geografic regions (sul, norte, etc).

So, user can double clik a node to load the data associated with him.


## Referências
- http://sigmajs.org/
- http://tsouk.com/html2graph
- https://github.com/kelvins/Municipios-Brasileiros

[sigmajs]: http://sigmajs.org/
