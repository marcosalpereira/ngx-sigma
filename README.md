# ngx-sigma

Read this in [english](https://gitlab.com/marcosalpereira/ngx-sigma/-/blob/master/README.en.md).

Biblioteca angular usando [sigmajs] para renderizar grafos dirigidos.

## Funcionalidades implementadas

- Mostrar imagem nos nós
- Mostrar nomes nas arestas
- *Drill down*. Possibilidade de clicar (duplo-clique) nos nós e anexar os dados referentes ao nó
- *Drill down* automático
- Layout dinâmico (usando o plugin ForceAtlas2)
- Salvar/restauras estado do gráfico
- Procurar por valores em qualquer propriedade nos dados e aplicar uma ação:
    - Marcar: Marca o nó com uma cor dinâmica
    - Marcar descendentes: Marca o nó e seus descendentes com uma cor dinâmica
    - Esconder: esconde o nó e seus descendentes
    - Recolher: recolhe (collapse) o nó e seus descendentes

## Aplicação exemplo

https://marcosalpereira.gitlab.io/ngx-sigma/

Essa aplicação carrega inicialmente as regiões geográfica brasileiras (sul, norte, etc).

O usuário pode solicitar para carregar mais dados realizando um duplo-click no nó desejado.


## Referências
- https://gitlab.com/marcosalpereira/ngx-sigma
- http://sigmajs.org/
- http://tsouk.com/html2graph
- https://github.com/kelvins/Municipios-Brasileiros

[sigmajs]: http://sigmajs.org/
