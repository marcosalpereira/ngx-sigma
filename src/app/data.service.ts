import { Injectable } from '@angular/core';
import { Graph } from 'ngx-sigma';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private http: HttpClient) { }

  getGraph(id: string): Observable<Graph> {
    return this.http.get<Graph>(`api/${id}`);
  }


}
