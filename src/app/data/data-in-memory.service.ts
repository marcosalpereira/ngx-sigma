import { InMemoryDbService } from 'angular-in-memory-web-api';
import { ESTADOS } from './estados';
import { MUNICIPIOS } from './municipios';

const REGIOES = [
  { id: 'norte' },
  { id: 'sul' },
  { id: 'nordeste' },
  { id: 'sudeste' },
  { id: 'centro oeste' }
];

export class DataInMemoryService implements InMemoryDbService {
  createDb() {
    const brasil =
    {
      name: 'Brasil',
      items: {
        nodes: [
          { id: 'brasil', imageUrl: 'assets/brasil.jpg' },
          ...REGIOES
        ],
        edges: [
          ...toEdge('Região', REGIOES, 'brasil', '2,4')
        ],
      },
    };


    const regioes = {};
    const estados = {};
    REGIOES.forEach(regiao => {
      const estadosRegiao = ESTADOS.filter((uf) => uf.regiao === regiao.id);
      regioes[regiao.id] =
      {
        name: regiao.id,
        items: {
          nodes: [
            { id: regiao.id },
            ...estadosRegiao
          ],
          edges: [
            ...toEdge('Estado', estadosRegiao, regiao.id)
          ]
        }
      };

      estadosRegiao.forEach(uf => {
        const municipios = MUNICIPIOS.filter(m => m.sigla_uf === uf.id);
        estados[uf.id] = {
          name: uf.id,
          items: {
            nodes: [
              {...uf},
              ...municipios
            ],
            edges: [
              ...toEdge('Município', municipios, uf.id)
            ]
          }
        }
      });
    });

    const ret = { brasil, ...regioes, ...estados };

    return ret;
  }
}

const toEdge = (label, arr, source, lineDash = undefined) => {
  return arr.map(o => {
    return { label, source, target: o.id, lineDash };
  });
};
