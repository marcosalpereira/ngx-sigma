﻿export const ESTADOS = [
  {

    id: 'RO',
    nome: 'Rondônia',
    tipo: 'uf',
    regiao: 'norte'
  },
  {

    id: 'AC',
    nome: 'Acre',
    tipo: 'uf',
    regiao: 'norte'
  },
  {

    id: 'AM',
    nome: 'Amazonas',
    tipo: 'uf',
    regiao: 'norte'
  },
  {

    id: 'RR',
    nome: 'Roraima',
    tipo: 'uf',
    regiao: 'norte'
  },
  {

    id: 'PA',
    nome: 'Pará',
    tipo: 'uf',
    regiao: 'norte'
  },
  {

    id: 'AP',
    nome: 'Amapá',
    tipo: 'uf',
    regiao: 'norte'
  },
  {

    id: 'TO',
    nome: 'Tocantins',
    tipo: 'uf',
    regiao: 'norte'
  },
  {

    id: 'MA',
    nome: 'Maranhão',
    tipo: 'uf',
    regiao: 'nordeste'
  },
  {

    id: 'PI',
    nome: 'Piauí',
    tipo: 'uf',
    regiao: 'nordeste'
  },
  {

    id: 'CE',
    nome: 'Ceará',
    tipo: 'uf',
    regiao: 'nordeste'
  },
  {

    id: 'RN',
    nome: 'Rio Grande do Norte',
    tipo: 'uf',
    regiao: 'nordeste'
  },
  {

    id: 'PB',
    nome: 'Paraíba',
    tipo: 'uf',
    regiao: 'nordeste'
  },
  {

    id: 'PE',
    nome: 'Pernambuco',
    tipo: 'uf',
    regiao: 'nordeste'
  },
  {

    id: 'AL',
    nome: 'Alagoas',
    tipo: 'uf',
    regiao: 'nordeste'
  },
  {

    id: 'SE',
    nome: 'Sergipe',
    tipo: 'uf',
    regiao: 'nordeste'
  },
  {

    id: 'BA',
    nome: 'Bahia',
    tipo: 'uf',
    regiao: 'nordeste'
  },
  {

    id: 'MG',
    nome: 'Minas Gerais',
    tipo: 'uf',
    regiao: 'sudeste'
  },
  {

    id: 'ES',
    nome: 'Espírito Santo',
    tipo: 'uf',
    regiao: 'sudeste'
  },
  {

    id: 'RJ',
    nome: 'Rio de Janeiro',
    tipo: 'uf',
    regiao: 'sudeste'
  },
  {

    id: 'SP',
    nome: 'São Paulo',
    tipo: 'uf',
    regiao: 'sudeste'
  },
  {

    id: 'PR',
    nome: 'Paraná',
    tipo: 'uf',
    regiao: 'sul'
  },
  {

    id: 'SC',
    nome: 'Santa Catarina',
    tipo: 'uf',
    regiao: 'sul'
  },
  {

    id: 'RS',
    nome: 'Rio Grande do Sul',
    tipo: 'uf',
    regiao: 'sul'
  },
  {

    id: 'MS',
    nome: 'Mato Grosso do Sul',
    tipo: 'uf',
    regiao: 'centro oeste'
  },
  {

    id: 'MT',
    nome: 'Mato Grosso',
    tipo: 'uf',
    regiao: 'centro oeste'
  },
  {

    id: 'GO',
    nome: 'Goiás',
    tipo: 'uf',
    regiao: 'centro oeste'
  },
  {

    id: 'DF',
    nome: 'Distrito Federal',
    tipo: 'uf',
    regiao: 'centro oeste',
  }
];
