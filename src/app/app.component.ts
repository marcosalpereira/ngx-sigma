import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from './data.service';
import { Graph, DataLoadedEvent, KeyWord, GraphOptions, SearchType, Filter, FilterType, GraphComponent, Operation, GraphState } from 'ngx-sigma';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  data: Graph;
  keyWords: KeyWord[] = [];

  options: GraphOptions = {
    appendMode: false,
    showNodeText: false,
    showEdgeText: false,
    dynamicLayout: true,
    autoExpand: false,
  };

  errorMessage: string;
  filterTypes: FilterType[];
  filters: Filter[];
  operations: Operation[];
  nodesLength: number;
  edgesLength: number;

  @ViewChild('graph', { static: false }) graphComponent: GraphComponent;

  private get filterManager() {
    return this.graphComponent.filterManager;
  }

  constructor(
    private dataService: DataService
  ) { }

  ngOnInit(): void {
    this.loadGraph('brasil');
  }

  public loadGraph(g) {
    this.errorMessage = '';
    this.dataService.getGraph(g)
      .subscribe(
        data => this.data = data,
        err => {
          if (this.options.autoExpand) {
            this.data = null;
          } else {
            this.errorMessage = 'Não existe dado associado';
          }
        }
      );
  }

  onNodeClick(node) {
    this.loadGraph(node.id);
  }

  onDataLoaded(event: DataLoadedEvent) {
    setTimeout(() => {
      this.keyWords = event.keyWords;
      this.filterTypes = event.filterTypes;
      this.filters = event.filters;
      this.operations = event.operations;
      this.nodesLength = event.nodesLength;
      this.edgesLength = event.edgesLength;
    }, 0);
  }

  onAddFilterClick(
    searchType: SearchType, filterType: FilterType, keyWord: KeyWord,
    inverse: any, operation: string, searchedValue: string) {
    this.errorMessage = '';
    const property = keyWord.propertyName;
    const filter = this.filterManager.findFilter(filterType, searchType, property, searchedValue);
    if (filter) {
      this.message('Filtro já existe!');
      return;
    }
    if (!this.filterManager.addFilter(filterType, searchType, property, inverse.checked, operation, searchedValue)) {
      this.message('Nenhum item encontrado');
    }
  }

  message(msg: string) {
    this.errorMessage = msg;
  }


  onSearchTypeChange(searchType: SearchType) {
    this.keyWords = this.filterManager.keyWords.filter(
      k => (
        (searchType === 'edge' && k.fromEdge) ||
        (searchType === 'node' && k.fromNode)
      ));
  }

  onKeyWordChange(keyWord: KeyWord) {

  }

  onChangeAppendMode(v: boolean) {
    this.options.appendMode = v;
    this.graphComponent.options = this.options;
  }

  onChangeShowNodeText(v: boolean) {
    this.options.showNodeText = v;
    this.graphComponent.options = this.options;
  }

  onChangeShowEdgeText(v: boolean) {
    this.options.showEdgeText = v;
    this.graphComponent.options = this.options;
  }

  onChangeDynamicLayout(v: boolean) {
    this.options.dynamicLayout = v;
    this.graphComponent.options = this.options;
  }

  onChangeAutoExpand(v: boolean) {
    this.options.autoExpand = v;
    this.graphComponent.options = this.options;
  }

  onFilterChange(filter: Filter, event) {
    this.filterManager.filterStatusChanged(filter, event.target.checked);
  }

  onRemoveFilterClick(filter: Filter) {
    this.filters = this.filterManager.removeFilter(filter);
  }

  onSaveState() {
    const state: GraphState = this.graphComponent.getState();
    const blob = new Blob([JSON.stringify(state)], { type: 'text/json' });
    const url = window.URL.createObjectURL(blob);

    const link = document.createElement('a');
    link.href = url;
    link.download = (state.graph.name || 'grafico') + '.json';
    link.click();
    }

  onLoadState(e) {
    const file = e.target.files[0];
    const fileReader = new FileReader();
    fileReader.onload = (lev) => {
      const str = fileReader.result as string;
      const state: GraphState = JSON.parse(str);
      this.graphComponent.setState(state);
    };
    fileReader.readAsText(file);

  }

}
