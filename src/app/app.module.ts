import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { DataInMemoryService } from './data/data-in-memory.service';
import { NgxSigmaModule } from 'ngx-sigma';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(DataInMemoryService, {
      passThruUnknownUrl: true,
      apiBase: 'api/'
    }),
    FormsModule,
    NgxSigmaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
