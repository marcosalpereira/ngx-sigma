import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { GraphNode, GraphOptions, DataLoadedEvent, OPERATIONS, GraphState, Graph, Filter } from './data.model';

import { Subscription } from 'rxjs';
import { GraphService } from './graph.service';

@Component({
  selector: 'ngs-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css'],
  providers: [GraphService]
})
export class GraphComponent implements OnChanges, OnInit, OnDestroy {
  @Input() data: Graph;
  @Output() nodeClick = new EventEmitter<GraphNode>();
  @Output() dataLoaded = new EventEmitter<DataLoadedEvent>();
  private _stateFilters: Filter[];

  @Input()
  set options(value: GraphOptions) {
    this.sigmaService.graphOptions = value;
  }

  private nodeclickedSubscription: Subscription;
  dataLoadedSubscription: Subscription;

  private get sigmaService() {
    return this.graphService.sigmaService;
  }

  public get filterManager() {
    return this.graphService.filterManager;
  }

  constructor(
    private graphService: GraphService) {
  }

  public ngOnInit(): void {
    this.nodeclickedSubscription = this.sigmaService.nodeclicked$
      .subscribe(node => this.nodeClick.emit(node));

    this.dataLoadedSubscription = this.sigmaService.dataLoaded$
      .subscribe(() => {
          if (this._stateFilters) {
            this.filterManager.restoreFilters(this._stateFilters);
            this._stateFilters = null;
          }
          this.dataLoaded.emit(this.createDataLoadEvent());
        });
  }

  public ngOnDestroy(): void {
    this.nodeclickedSubscription.unsubscribe();
    this.dataLoadedSubscription.unsubscribe();
  }

  public setState(state: GraphState) {
    this.data = state.graph;
    if (state.filters && state.filters.length) {
      this._stateFilters = state.filters;
    }
    this.sigmaService.loadGraph(this.data, 'graph-container', true);
  }

  public getState(): GraphState {
    const state: GraphState = {
      breadcrumbs: [],
      filters: this.filterManager.filters,
      graph: {
        id: this.data.id,
        name: this.data.name,
        items: {
          edges: this.sigmaService.edges(),
          nodes: this.sigmaService.nodes(),
        }
      }
    };
    return state;
  }

  public ngOnChanges(changes: SimpleChanges): void {
    const dataChange = changes.data;
    if (dataChange.currentValue) {
      const graph = dataChange.currentValue;
      this.sigmaService.loadGraph(graph, 'graph-container');
    }
  }

  private createDataLoadEvent(): DataLoadedEvent {
    const e = {
      keyWords: this.filterManager.keyWords,
      filterTypes: this.filterManager.filterTypes,
      filters: this.filterManager.filters,
      operations: OPERATIONS,
      nodesLength: this.sigmaService.nodes().length,
      edgesLength: this.sigmaService.edges().length
    };
    return e;
  }


}

