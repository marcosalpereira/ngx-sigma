export const log = (o) => {
    console.log(JSON.parse(JSON.stringify(o)));
};

export function getOrCreate<T>(map: Map<string, T>, key: string, notExistsFn: () => T): T {
    if (!map.has(key)) {
        map.set(key, notExistsFn());
    }
    return map.get(key);
}

export function setAddAll(set: Set<any>, items: Set<any> | any[]) {
    items.forEach(item => set.add(item));
}

export function generateId(): string {
    return new Date().getTime() + '';
}



