export type SearchType = 'node' | 'edge';
export type FilterType = 'marcar' | 'marcar descendentes' | 'esconder' | 'recolher';

export type GraphNode = INode | any;
export type GraphEdge = IEdge | any;

interface INode {
    id: string;
    name?: string;
    title?: string;
    imageUrl?: string;
    faicon: string;
}

interface IEdge {
    source: number;
    target: number;
    name?: string;
}

export interface SubGraph {
    nodes: Set<GraphNode>;
    edges: Set<GraphEdge>;
}

export interface GraphOptions {
    appendMode: boolean;
    showNodeText: boolean;
    showEdgeText: boolean;
    dynamicLayout: boolean;
    autoExpand: boolean;
}

export interface GraphItems {
    nodes: GraphNode[];
    edges: GraphEdge[];
}

export interface Graph {
    id: string;
    name: string;
    items: GraphItems;
}

export interface KeyWord {
    label: string;
    propertyName: string;
    fromNode?: boolean;
    fromEdge?: boolean;
}

export const OPERATIONS: Operation[] = ['=~', '=', '>', '<', '!=', '>=', '<='];

export interface DataLoadedEvent {
    keyWords: KeyWord[];
    filterTypes: FilterType[];
    operations: Operation[];
    filters: Filter[];
    nodesLength: number;
    edgesLength: number;
}

export type Operation = '=~' | '=' | '>' | '<' | '!=' | '>=' | '<=';

export interface FilterItems {
    affectedItems: SubGraph;
    sourceNodes: GraphNode[];
}

export interface FilterItemsIds {
    sourceNodesIds: string[];
    edgesIds: string[];
    nodesIds: string[];
}

export interface Filter {
    id?: string;
    nodesLength: number;
    itemsIds?: FilterItemsIds;
    sourceNodesLength?: number;
    active: boolean;
    filterType: FilterType;
    searchType: SearchType;
    propertyLabel: string;
    propertyName: string;
    operation: string;
    inverse: boolean;
    searchedValue: string;
    color?: string;
}

export interface GraphState {
    breadcrumbs: string[];
    filters: Filter[];
    graph: Graph;
}
