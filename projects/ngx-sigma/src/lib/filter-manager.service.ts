import { GraphEdge, GraphNode, GraphItems, Filter, KeyWord, SubGraph, FilterType, SearchType, FilterItems, FilterItemsIds } from './data.model';
import { setAddAll, getOrCreate, generateId } from './util';
import { GraphService } from './graph.service';
import { translate } from './translate.util';

const IGNORED_PROPERTIES = [
    'size', 'id', 'imageUrl',
    'color', 'index', 'url', 'type',
    new RegExp('read_cam.+'), new RegExp('renderer.+')];

const HIGHLIGHT_COLORS = ['#ff0f0f', '#4bff0f', '#0f5bff', '#5b0fff'];

export class FilterManagerService {
    filterTypes: FilterType[] = ['marcar', 'marcar descendentes', 'esconder', 'recolher'];
    filters: Filter[] = [];
    keyWords: KeyWord[];

    highliteColorIndex = 0;

    private dataKeys: Map<string, KeyWord>;

    private get sigmaService() {
        return this.graphService.sigmaService;
    }

    constructor(
        private graphService: GraphService) { }

    filterStatusChanged(filter: Filter, active: boolean) {
        filter.active = active;
        this.applyFilter(filter);
    }

    public restoreFilters(filters: Filter[]) {
        this.removeAllFilters();
        filters.forEach(filter => this.restoreFilter(filter));
    }

    private restoreFilter(filter: Filter) {
        this.filters.push(filter);
        this.applyFilter(filter);
        if (filter.color) {
            this.advanceColorIndex();
        }
    }

    private applyFilter(filter: Filter) {
        const filterItems = this.getFilterItems(filter.itemsIds);
        this.sigmaService.applyFilter(filter, filterItems);
    }

    public removeAllFilters() {
        this.highliteColorIndex = 0;
        this.filters = [];
    }

    public removeFilter(filter: Filter): Filter[] {
        if (filter.active) {
          filter.active = false;
          const filterItems = this.getFilterItems(filter.itemsIds);
          this.sigmaService.applyFilter(filter, filterItems);

        }
        if (filter.color) {
            this.advanceColorIndex(-1);
        }

        this.filters = this.filters.filter(f => f !== filter);
        return this.filters;
    }

    public findFilter(filterType: FilterType, searchType: SearchType, property: string, searchedValue: string): Filter {
        return this.filters.find(f =>
            f.filterType === filterType
            && f.searchType === searchType
            && f.propertyName === property
            && f.searchedValue === searchedValue);
    }

    public addFilter(
        filterType: FilterType, searchType: SearchType,
        property: string, inverse: boolean, operation: string, searchedValue: string): boolean {
        if (filterType === 'esconder') {
            return this.searchAndHide(searchType, property, inverse, operation, searchedValue);
        } else if (filterType === 'recolher') {
            return this.searchAndCollapse(searchType, property, inverse, operation, searchedValue);
        } else if (filterType === 'marcar') {
            return this.searchAndMark(searchType, property, inverse, operation, searchedValue);
        } else if (filterType === 'marcar descendentes') {
            return this.searchAndMarkDescending(searchType, property, inverse, operation, searchedValue);
        }
    }

    private collectEdgesFromNode(node: GraphNode, collecteds: GraphEdge[]) {

        // edges from node (no cycles)
        const edgesFromNode = this.sigmaService.edges()
            .filter(e => e.source.id === node.id);

        collecteds.push(...edgesFromNode);
        edgesFromNode.forEach(e => {
            if (!collecteds.find(collected => collected.source.id === e.target.id)) {
                this.collectEdgesFromNode(e.target, collecteds);
            }
        });
    }

    public findOrCreateFilter(
        filterType: FilterType, searchType: SearchType,
        property: string, inverse: boolean, operation: string, searchedValue: string): Filter {
        let filter = this.findFilter(filterType, searchType, property, searchedValue);
        if (!filter) {
            filter = this.createFilter(property, filterType, searchType, inverse, operation, searchedValue);
        }
        return filter;
    }

    private getCollapseAffectedItems(inverse: boolean, sourceNodes: GraphNode[]): SubGraph {
        const affectedNodes = new Set<GraphNode>();
        const affectedEdges = new Set<GraphEdge>();

        for (const node of sourceNodes) {
            const sg = this.sigmaService.getSubGraph(node);
            setAddAll(affectedNodes, sg.nodes);
            setAddAll(affectedEdges, sg.edges);
        }

        const ret = {
            nodes: affectedNodes,
            edges: affectedEdges
        };
        if (inverse) {
            return this.invert(ret);
        }
        return ret;
    }

    invert(sg: SubGraph): SubGraph {
        return {
            nodes: new Set(this.sigmaService.nodes().filter(node => !sg.nodes.has(node))),
            edges: new Set(this.sigmaService.edges().filter(edge => !sg.edges.has(edge)))
        };
    }

    private getHideAffectedItems(inverse: boolean, sourceNodes: GraphNode[]): SubGraph {
        const affectedNodes = new Set<GraphNode>();
        const affectedEdges = new Set<GraphEdge>();

        for (const node of sourceNodes) {
            const sg = this.sigmaService.getSubGraph(node);
            setAddAll(affectedNodes, sg.nodes);
            affectedNodes.add(node);

            setAddAll(affectedEdges, sg.edges);
            setAddAll(affectedEdges, this.sigmaService.getEdgesPointingThisNode(node));
        }

        const ret = {
            nodes: affectedNodes,
            edges: affectedEdges
        };
        if (inverse) {
            return this.invert(ret);
        }
        return ret;

    }

    private getOrCreateKey(dataKeys: Map<string, KeyWord>, propertyName: string): KeyWord {
        const keyWord: KeyWord = getOrCreate(dataKeys, propertyName, () => {
            return {
                propertyName,
                label: translate(propertyName),
                values: new Set()
            };
        });
        return keyWord;
    }

    public collectKeys(items: GraphItems, appendMode) {
        const dataKeys = this.dataKeys || new Map();
        if (!appendMode || !dataKeys) {
            dataKeys.clear();
        }

        this.indexItems(dataKeys, items.nodes, (keyWord) => keyWord.fromNode = true);
        this.indexItems(dataKeys, items.edges, (keyWord) => keyWord.fromEdge = true);

        this.dataKeys = dataKeys;
        this.keyWords = [...dataKeys.values()];

    }


    private indexItems(dataKeys: Map<string, KeyWord>, items: any[], callbackfn: (keyWord: KeyWord) => void) {
        items.forEach(item => {
            for (const name in item) {
                if (!this.isIgnoredProperty(name) && item.hasOwnProperty(name)) {
                    const keyWord = this.getOrCreateKey(dataKeys, name);
                    callbackfn(keyWord);
                }
            }
        });
    }

    private isIgnoredProperty(name: string) {
        if (name.length <= 2 || name.startsWith('_')) {
            return true;
        }
        for (const p of IGNORED_PROPERTIES) {
            if (name.match(p) !== null) {
                return true;
            }
        }
        return false;
    }

    private findNodes(nodes: GraphNode[], property: string, operation: string, searched: string | RegExp): GraphNode[] {
        return nodes.filter(node => {
            return this.compliesWith(node, property, operation, searched);
        });
    }

    private compliesWith(obj: any, property: string, operation: string, searched: string | RegExp) {
        let objValue = obj[property];

        // se a propriedade nao foi encontrada, verficar se está pesquisando pelo 'name'
        // como ela é opcional, nesse caso, usar o 'id'
        if (!objValue && property === 'name') {
            objValue = obj.id;
        }

        if (!objValue) { return false; }

        // se for um Node
        if (objValue.id) {
            // pegar o nome dele
            objValue = this.nodeName(objValue);
        }

        let complies: boolean;
        if (Number.isInteger(objValue)) {
            complies = this.numberCompliesWith(operation, objValue, +searched);
        } else {
            complies = this.stringCompliesWith(operation, objValue.toLowerCase(), searched);
        }
        return complies;
    }

    stringCompliesWith(operation: string, objValue: string, searched: string | RegExp): boolean {
        if (operation === '=') { return objValue === searched; }
        if (operation === '>') { return objValue > searched; }
        if (operation === '<') { return objValue < searched; }

        if (operation === '!=') { return objValue !== searched; }
        if (operation === '>=') { return objValue >= searched; }
        if (operation === '<=') { return objValue <= searched; }

        if (operation === '=~') { return objValue.match(searched) != null; }
        return false;
    }

    numberCompliesWith(operation: string, objValue: number, searched: number | RegExp): boolean {
        if (operation === '=') { return objValue === searched; }
        if (operation === '>') { return objValue > searched; }
        if (operation === '<') { return objValue < searched; }

        if (operation === '!=') { return objValue !== searched; }
        if (operation === '>=') { return objValue >= searched; }
        if (operation === '<=') { return objValue <= searched; }
        return false;
    }

    private findNodesByEdges(
        edges: GraphEdge[], property: string,
        operation: string, searched: string | RegExp): GraphNode[] {
        const edgesFiltered = edges.filter(edge => {
            return this.compliesWith(edge, property, operation, searched);
        });
        return edgesFiltered.map(edge => edge.source);
    }

    private nodeName(node: any) {
        return node.name || node.id;
    }

    public collapseNode(node: GraphNode) {
        const filter = this.findOrCreateFilter('recolher', 'node', 'label', false, '=', this.nodeName(node));
        let filterItems: FilterItems;
        if (filter.id) {
            filter.active = !node._collapsed;
            filterItems = this.getFilterItems(filter.itemsIds);
        } else {
            filterItems = {
                sourceNodes: [node],
                affectedItems: this.getCollapseAffectedItems(false, [node])
            };
            this.saveFilter(filter, filterItems);
        }
        this.sigmaService.applyFilter(filter, filterItems);
    }

    private searchAndCollapse(searchType: SearchType, property: string, inverse: boolean, operation: string, searchedValue: string) {
        const sourceNodes: GraphNode[] = this.searchNodes(searchType, property, operation, searchedValue);
        if (sourceNodes.length === 0) {
            return false;
        }
        const affectedItems = this.getCollapseAffectedItems(inverse, sourceNodes);
        const filterItems = {sourceNodes, affectedItems};
        const filter = this.createFilter(property, 'recolher', searchType, inverse, operation, searchedValue);
        this.saveFilter(filter, filterItems);
        this.sigmaService.applyFilter(filter, filterItems);
        return true;
    }

    private saveFilter(filter: Filter, filterItems: FilterItems) {
        filter.id = generateId();
        filter.itemsIds = this.getItemsIds(filterItems);
        this.filters.push(filter);
    }

    private getItemsIds(filterItems: FilterItems): FilterItemsIds {
        return {
            sourceNodesIds: filterItems.sourceNodes.map(node => node.id),
            edgesIds: [...filterItems.affectedItems.edges.keys()].map(edge => edge.id),
            nodesIds: [...filterItems.affectedItems.nodes.keys()].map(node => node.id)
        };
    }

    private getFilterItems(itemsIds: FilterItemsIds): FilterItems {
        return {
            sourceNodes: itemsIds.sourceNodesIds.map(nodeId => this.sigmaService.nodes(nodeId)),
            affectedItems: {
                edges: new Set(itemsIds.edgesIds.map(edgeId => this.sigmaService.edges(edgeId))),
                nodes: new Set(itemsIds.nodesIds.map(nodeId => this.sigmaService.nodes(nodeId)))
            }
        };
    }

    private createFilter(
            propertyName: string, filterType: FilterType, searchType: SearchType,
            inverse: boolean, operation: string, searchedValue: string, color?: string): Filter {
        const propertyLabel = translate(propertyName);
        const filter: Filter = {
            nodesLength: this.sigmaService.nodes().length,
            active: true,
            filterType,
            searchType,
            propertyLabel,
            propertyName,
            inverse,
            operation,
            searchedValue,
            color
        };
        return filter;
    }

    private searchAndHide(searchType: SearchType, property: string, inverse: boolean, operation: string, searchedValue: string) {
        const sourceNodes: GraphNode[] = this.searchNodes(searchType, property, operation, searchedValue);
        if (sourceNodes.length === 0) {
            return false;
        }
        const affectedItems = this.getHideAffectedItems(inverse, sourceNodes);
        const filterItems = {sourceNodes, affectedItems};
        const filter = this.createFilter(property, 'esconder', searchType, inverse, operation, searchedValue);
        this.saveFilter(filter, filterItems);
        this.sigmaService.applyFilter(filter, filterItems);
        return true;
    }

    private searchAndMarkDescending(searchType: SearchType, property: string, inverse: boolean, operation: string, searchedValue: string) {

        const sourceNodes: GraphNode[] = this.searchNodes(searchType, property, operation, searchedValue);
        if (sourceNodes.length === 0) {
            return false;
        }
        const affectedItems = this.getHideAffectedItems(inverse, sourceNodes);
        // TODO create getSubNodes
        affectedItems.edges = new Set();

        const filterItems = {sourceNodes, affectedItems};
        const filter = this.createFilter(
            property, 'marcar descendentes', searchType,  inverse,
            operation, searchedValue, this.getNextColor());

        this.saveFilter(filter, filterItems);
        this.sigmaService.applyFilter(filter, filterItems);
        return true;
    }

    private searchAndMark(searchType: SearchType, property: string, inverse: boolean, operation: string, searchedValue: string) {

        const sourceNodes: GraphNode[] = this.searchNodes(searchType, property, operation, searchedValue);
        if (sourceNodes.length === 0) {
            return false;
        }

        let affectedNodes = new Set(sourceNodes);
        if (inverse) {
            affectedNodes = new Set(this.sigmaService.nodes().filter(node => !affectedNodes.has(node)));
        }
        const affectedItems = {
            edges: new Set(),
            nodes: affectedNodes
        };
        const filterItems = {sourceNodes, affectedItems};
        const filter = this.createFilter(
            property, 'marcar', searchType, inverse, operation,
            searchedValue, this.getNextColor());

        this.saveFilter(filter, filterItems);
        this.sigmaService.applyFilter(filter, filterItems);
        return true;
    }

    private getNextColor(): string {
        const color = HIGHLIGHT_COLORS[this.highliteColorIndex];
        this.advanceColorIndex();
        return color;
    }

    private advanceColorIndex(incr = 1) {
        this.highliteColorIndex =
            (this.highliteColorIndex + incr) % HIGHLIGHT_COLORS.length;
    }

    private searchNodes(
        searchType: SearchType, property: string, operation: string, searchedValue: string): GraphNode[] {
        const searched: string | RegExp =
            operation === '=~'
                ? new RegExp(searchedValue, 'i')
                : searchedValue.toLowerCase();

        let nodes: GraphNode[];
        if (searchType === 'node') {
            nodes = this.findNodes(this.sigmaService.nodes(), property, operation, searched);
        }
        else if (searchType === 'edge') {
            nodes = this.findNodesByEdges(this.sigmaService.edges(), property, operation, searched);
        }
        return nodes;
    }

}
