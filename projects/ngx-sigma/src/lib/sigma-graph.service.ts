import { Graph, GraphEdge, GraphNode, GraphOptions, Filter, SubGraph, FilterItems } from './data.model';
import { Subject } from 'rxjs';
import { getOrCreate } from './util';
import { sigma } from 'sigma';
import { GraphService } from './graph.service';
declare const sigma: any;

const graphSettings = {
    rescaleIgnoreSize: false,
    autoRescale: false,
    drawLabels: false,
    drawEdges: true,
    sideMargin: 10,
    clone: false,
    doubleClickEnabled: false,
    minArrowSize: 4,
    labelThreshold: 0,
    drawEdgeLabels: false,
    edgeLabelSize: 'proportional',
    zoomMax: 300
};
const forceAtlas2Config = {
    linLogMode: true,
    outboundAttractionDistribution: false,
    adjustSizes: true,
    edgeWeightInfluence: 1,
    scalingRatio: 20,
    strongGravityMode: false,
    gravity: 3,
    barnesHutOptimize: true,
    barnesHutTheta: 0.5,
    slowDown: 10,
    startingIterations: 1,
    iterationsPerRender: 1,
    worker: true
};

const NODE_DEF_COLOR = 'lightgray';
export class SigmaGraphService {
    private nodeClickedSubject = new Subject<GraphNode>();
    nodeclicked$ = this.nodeClickedSubject.asObservable();

    private dataLoadedSubject = new Subject<void>();
    dataLoaded$ = this.dataLoadedSubject.asObservable();

    private nodeClicked: GraphNode;
    private options: GraphOptions;
    startDragY: any;
    startDragX: any;
    dragStartX: any;
    dragStartY: any;

    private get filterManager() {
        return this.graphService.filterManager;
    }
    private set sigmaInstance(s) {
        this.graphService.sigmaInstance = s;
    }
    private get sigmaInstance() {
        return this.graphService.sigmaInstance;
    }

    constructor(
        private graphService: GraphService) {}

    public edges(id?: string) {
        if (!this.sigmaInstance) { return []; }
        return id ? this.sigmaInstance.graph.edges(id) : this.sigmaInstance.graph.edges();
    }

    public nodes(id?: string) {
        if (!this.sigmaInstance) { return []; }
        return id ? this.sigmaInstance.graph.nodes(id) : this.sigmaInstance.graph.nodes();
    }

    public set graphOptions(options: GraphOptions) {
        const dynamicLayoutChanged = !this.options || this.options.dynamicLayout !== options.dynamicLayout;
        this.options = {...options};
        if (this.sigmaInstance) {
            this.sigmaInstance.settings('drawLabels', options.showNodeText);
            this.sigmaInstance.settings('drawEdgeLabels', options.showEdgeText);

            if (dynamicLayoutChanged) {
                this.setDynamicLayoutStatus(options.dynamicLayout);
            } else {
                this.sigmaInstance.refresh();
            }

            if (options.autoExpand) {
                options.appendMode = true;
                setTimeout(this.doAutoExpand, 200);
            }
        }
    }

    doAutoExpand = () => {
        const node = this.getFirstNodeNotExpanded();
        if (node) {
            this.nodeClicked = node;
            this.nodeClickedSubject.next(node);
        } else {
            this.options.autoExpand = false;
        }
    }

    getFirstNodeNotExpanded() {
        const nodes: GraphNode[] = this.sigmaInstance.graph.nodes();
        return nodes.find(n => !n._autoExpanded && this.isLeaf(n));
    }

    private setDynamicLayoutStatus(active: boolean) {
        if (active) {
            this.sigmaInstance.killForceAtlas2();
            this.sigmaInstance.startForceAtlas2(forceAtlas2Config);
        } else {
            this.sigmaInstance.stopForceAtlas2();
        }
    }

    private nodeName(node: any) {
        return node.name || node.nome || node.id;
    }
    private nodeSize = (node) => {
        return node.size ? node.size : 5;
    }

    private nodeColor(d: GraphNode) {
        return d.color ? d.color : NODE_DEF_COLOR;
    }

    onNodeClicked = (event) => {
        const node: GraphNode = event.data.node;
        const isLeaf = this.isLeaf(node);
        if (node._collapsed || !isLeaf) {
            this.filterManager.collapseNode(node);
        } else {
            this.nodeClicked = node;
            this.nodeClickedSubject.next(node);
        }
    }

    private isLeaf(node: any) {
        return this.sigmaInstance.graph.degree(node.id, 'out') === 0;
    }

    public loadGraph(graph: Graph, container: string, restoreMode = false) {
        if (this.options.autoExpand && !graph) {
            this.doAutoExpand();
        }
        const imagesUrls = graph.items.nodes.filter(node => node.imageUrl);
        // Se tiver imagem, tem que usar um renderer especial definido no final desse arquivo.
        // TODO isolar o image renderer num arquivo a parte.
        if (imagesUrls && imagesUrls.length) {
            this.loadImages(imagesUrls, () => {
                this._loadGraph(graph, container, restoreMode);
            });
        } else {
            this._loadGraph(graph, container, restoreMode);
        }
    }

    private loadImages(imagesUrls, callbackFn: () => void) {
        imagesUrls.forEach((node, imageIndex) => {
            sigma.canvas.nodes.image.cache(node.imageUrl, () => {
                if (imageIndex + 1 === imagesUrls.length) {
                    callbackFn();
                }
            });
        });
    }

    private _loadGraph = (graph: Graph, container: string, restoreMode: boolean) => {
        graphSettings.drawLabels = this.options.showNodeText;
        graphSettings.drawEdgeLabels = this.options.showEdgeText;

        if (!this.sigmaInstance) {
            this.initSigmaInstance(container);
        }

        if (restoreMode) {
            this.sigmaInstance.stopForceAtlas2();
        }

        const appendItems = this.nodeClicked && (this.options.appendMode || this.options.autoExpand);
        if (!appendItems || restoreMode) {
            this.sigmaInstance.graph.clear();
        } else {
            this.resetNodesColors();
        }

        graph.items.nodes.forEach(node => {
            this.initNode(node, appendItems, restoreMode);
        });
        graph.items.edges.forEach(edge => {
            this.initEdge(edge, restoreMode);
        });

        if (!restoreMode) {
            this.layoutGraph(graph);
        }

        this.filterManager.removeAllFilters();
        this.filterManager.collectKeys(graph.items, this.options.appendMode);

        this.draw(restoreMode === false);

        this.dataLoadedSubject.next();

        if (this.options.autoExpand) {
            this.doAutoExpandAgain(graph);
        }
    }

    private doAutoExpandAgain(graph: Graph) {
        const wait = Math.min(graph.items.nodes.length * 70, 5000);
        setTimeout(this.doAutoExpand, wait);
    }

    private initEdge(edge: GraphEdge, restoreMode: boolean) {
        if (restoreMode) {
            this.sigmaInstance.graph.addEdge(edge);
        }
        else {
            edge.id = edge.source + edge.target;
            edge.type = 'arrow';
            this.sigmaInstance.graph.addEdge(edge);
        }
    }

    private initNode(node: GraphNode, appendItems: boolean, restoreMode: boolean) {
        if (restoreMode) {
            delete node._savedColor;
            delete node._collapsed;
            delete node._autoExpanded;
            node.color = NODE_DEF_COLOR;
            this.sigmaInstance.graph.addNode(node);
            return;
        }
        node.type = 'custom';
        node.size = this.nodeSize(node);
        node.color = this.nodeColor(node);
        node.label = this.nodeName(node);
        delete node.name;
        delete node.nome;

        if (node.imageUrl) {
            node.url = node.imageUrl;
            node.type = 'image';
            delete node.imageUrl;
        }
        if (!(appendItems && this.nodeClicked.id === node.id)) {
            this.sigmaInstance.graph.addNode(node);
        }
    }

    private initSigmaInstance(container: string) {
        this.sigmaInstance = new sigma({
            renderer: {
                container: document.getElementById(container),
                type: 'canvas'
            },
            settings: graphSettings
        });
        this.sigmaInstance.bind('doubleClickNode', this.onNodeClicked);

        const dragListener = sigma.plugins.dragNodes(this.sigmaInstance, this.sigmaInstance.renderers[0]);
        dragListener.bind('startdrag', (e) => {
            this.dragStartX = e.data.node.x;
            this.dragStartY = e.data.node.y;
            if (this.sigmaInstance.isForceAtlas2Running()) {
                this.sigmaInstance.stopForceAtlas2();
            }
        });
        dragListener.bind('drop', (e) => {
            const nodes = this.getLeavingEdges(e.data.node)
                .map(edge => this.sigmaInstance.graph.nodes(edge.target))
                .filter(node => this.isLeaf(node));

            let radius = 0;
            nodes.forEach(node => {
                const r = Math.max( Math.abs(this.dragStartX - node.x), Math.abs(this.dragStartY - node.y));
                radius = Math.max(radius, r);
            });

            this.radialLayout(e.data.node, nodes, radius, 1);
            this.sigmaInstance.refresh();
        });
    }

    private resetNodesColors() {
        this.sigmaInstance.graph.nodes().forEach(node => {
            node.color = node._savedColor && node._savedColor.length
                        ? node._savedColor[0]
                        : NODE_DEF_COLOR;
            delete node._savedColor;
        });
    }

    private draw(startAtlas = true) {
        this.sigmaInstance.refresh();
        if (startAtlas) {
            this.sigmaInstance.killForceAtlas2();
            this.sigmaInstance.startForceAtlas2(forceAtlas2Config);
        }
    }

    private layoutGraph(graph: Graph) {
        const edgesGrouped = this.groupEdges(graph.items.edges);
        for (const groupKey of edgesGrouped.keys()) {
            this.layoutGroup(groupKey, edgesGrouped.get(groupKey));
        }
    }

    private groupEdges(edges): Map<string, string[]> {
        const map: Map<string, string[]> = new Map();
        for (const edge of edges) {
            const edgeNodes = getOrCreate(map, edge.source, () => []);
            edgeNodes.push(edge.target);
        }
        return map;
    }

    private layoutGroup(rootNodeId: string, nodesIds: string[]) {
        const radius = Math.max(10, 0.2 * nodesIds.length);

        const rootNode = this.sigmaInstance.graph.nodes(rootNodeId);
        if (rootNode._layouted) {
            const refNode = rootNode._parent;
            const up = rootNode.y > refNode.y;
            const right = rootNode.x > refNode.x;

            rootNode.x += right ? radius : -radius;
            rootNode.y += up ? radius : -radius;
        } else {
            rootNode.x = 0;
            rootNode.y = 0;
        }
        rootNode._layouted = true;

        const nodes = nodesIds.map(id => this.sigmaInstance.graph.nodes(id));

        // dispose itens in 3 concentric layers around the root
        this.radialLayout(rootNode, nodes, radius, 3);
    }

    private radialLayout(rootNode: GraphNode, nodes: GraphNode[], radius: number, concentricLayers: number) {
        const groups = this.groupElements(nodes.length, concentricLayers);

        const radiusStep = radius / concentricLayers;
        const step = 360.0 / groups[0];
        let index = 0;
        for (const qtElements of groups) {
            let angle = 360 * Math.random();
            for (let i = 0; i < qtElements; i++) {
                const node = nodes[index];
                node.x = rootNode.x + Math.cos(randomShift(angle) * Math.PI / 180.0) * radius;
                node.y = rootNode.y + Math.sin(randomShift(angle) * Math.PI / 180.0) * radius;
                node._parent = rootNode;
                node._layouted = true;
                angle += step;
                index++;
            }
            radius -= radiusStep;
        }

    }

    /**
     * Separate elements in groupCount groups.
     */
    private groupElements(elementsCount: number, groupCount: number) {
        const groups = [];
        let len = elementsCount;
        const m = Math.floor(Math.max(Math.min(groupCount, len), len / groupCount));
        groups[0] = m;
        len -= groups[0];
        groups[1] = len > m ? m : len;
        len -= groups[1];
        groups[2] = len > m ? m : len;
        groups[2] += elementsCount - groups[0] - groups[1] - groups[2];
        return groups;
    }

    public applyFilter(filter: Filter, filterItems: FilterItems) {
        const affectedNodes = filterItems.affectedItems.nodes;
        const affectedEdges = filterItems.affectedItems.edges;
        const sourceNodes = filterItems.sourceNodes;

        if (filter.filterType === 'recolher' || filter.filterType === 'esconder') {
            affectedNodes.forEach(node => node.hidden = filter.active);
            affectedEdges.forEach(edge => edge.hidden = filter.active);

            if (filter.filterType === 'recolher') {
                sourceNodes.forEach(node => node._collapsed = filter.active);
            }

        } else if (filter.filterType === 'marcar' || filter.filterType === 'marcar descendentes') {
            affectedNodes.forEach(node => {
                if (filter.active) {
                    if (!node._savedColor) { node._savedColor = []; }

                    node._savedColor.push(node.color);
                    node.color = filter.color;
                } else {
                    node.color = node._savedColor && node._savedColor.length
                        ? node._savedColor.pop()
                        : NODE_DEF_COLOR;
                }
            });

        }

        this.draw(false);
    }

    private getLeavingEdges(node: GraphNode): GraphEdge[] {
        if (this.isLeaf(node)) {
            return [];
        }
        const edges: GraphEdge[] = this.edges()
            .filter(e => e.source === node.id);
        return edges;
    }

    public getEdgesPointingThisNode(node: GraphNode): GraphEdge[] {
        return this.edges().filter(
            edge => edge.target === node.id);
    }

    public getSubGraph(root: GraphNode): SubGraph {
        const nodes: Set<GraphNode> = new Set();
        const edges: Set<GraphEdge> = new Set();
        this._getSubGraph(root, nodes, edges);
        const subGraph: SubGraph = { edges, nodes };
        return subGraph;
    }

    private _getSubGraph(root: GraphNode, collectedNodes: Set<GraphNode>, collectedEdges: Set<GraphEdge>) {
        const leavingEdges = this.getLeavingEdges(root);
        for (const edge of leavingEdges) {
            collectedEdges.add(edge);
            const nodeTarget = this.sigmaInstance.graph.nodes(edge.target);
            if (!collectedNodes.has(nodeTarget)) {
                collectedNodes.add(nodeTarget);
                this._getSubGraph(nodeTarget, collectedNodes, collectedEdges);
            }
        }
    }

}

const randomShift = (base) => {
    const delta = Math.random() * 6 * randomSign();
    return base + delta;
};

const randomSign = () => {
    return Math.random() >= 0.5 ? 1 : -1;
};

/**
 * This is a basic example on how to develop a custom node renderer. In
 * this example, the renderer will display an image clipped in a disc,
 * with a border colored according the node's "color" value.
 *
 * If a node as the value "image" to its attribute "type", then it will
 * displayed with the node renderer "sigma.canvas.nodes.image", with the
 * url being its "url" value.
 *
 * IMPORTANT: This node renderer just works with the canvas renderer. If
 * you do want to display images with the WebGL renderer, you will have
 * to develop a specific WebGL node renderer.
 */
// sigma.utils.pkg('sigma.canvas.nodes');
sigma.canvas.nodes.image = (() => {
    const _cache = {};
    const _loading = {};
    const _callbacks = {};

    const renderer = (node, context, settings) => {
        const prefix = settings('prefix') || '';
        const size = node[prefix + 'size'];
        const url = node.url;

        const x = node[prefix + 'x'];
        const y = node[prefix + 'y'];

        if (_cache[url]) {
            // context.save();

            // Draw the clipping disc:
            context.beginPath();
            context.arc(x, y, size + 3, 0, Math.PI * 2, true);
            context.closePath();
            context.fillStyle = node.color || settings('defaultNodeColor');
            context.fill();

            // Draw the image
            context.drawImage(
              _cache[url],
              x - size, y - size, 2 * size, 2 * size
            );

        } else {
            sigma.canvas.nodes.image.cache(url);
            sigma.canvas.nodes.def.apply(
                sigma.canvas.nodes,
                node, context, settings
            );
        }
    };

    // Let's add a public method to cache images, to make it possible to
    // preload images before the initial rendering:
    renderer.cache = (url, callback) => {
        if (callback) {
            _callbacks[url] = callback;
        }

        if (_loading[url]) { return; }

        const img = new Image();

        img.onload = function() {
            _loading[url] = false;
            _cache[url] = img;

            if (_callbacks[url]) {
                _callbacks[url].call(this, img);
                delete _callbacks[url];
            }
        };

        _loading[url] = true;
        img.src = url;
    };

    return renderer;
})();

sigma.canvas.nodes.custom = (() => {
    const renderer = (node, context, settings) => {
        const prefix = settings('prefix') || '';
        const size = node[prefix + 'size'];

        const x = node[prefix + 'x'];
        const y = node[prefix + 'y'];

        if (node._collapsed) {
            context.fillStyle = '#6b30e3';
            context.beginPath();
            context.arc(x, y, size + 3, 0, Math.PI * 2, true);
            context.closePath();
            context.fill();

            context.fillText('+', x - 4, y + size + 12);
        }
        context.fillStyle = node.color || settings('defaultNodeColor');
        context.beginPath();
        context.arc(x, y, size, 0, Math.PI * 2, true);
        context.closePath();
        context.fill();
    };


    return renderer;
})();

sigma.canvas.edges.arrow = (edge, source, target, context, settings) => {
    let color = edge.color;
    const prefix = settings('prefix') || '';
    const edgeColor = settings('edgeColor');
    const defaultNodeColor = settings('defaultNodeColor');
    const defaultEdgeColor = settings('defaultEdgeColor');
    const size = edge[prefix + 'size'] || 1;
    const tSize = target[prefix + 'size'];
    const sX = source[prefix + 'x'];
    const sY = source[prefix + 'y'];
    const tX = target[prefix + 'x'];
    const tY = target[prefix + 'y'];
    const aSize = Math.max(size * 2.5, settings('minArrowSize'));
    const d = Math.sqrt(Math.pow(tX - sX, 2) + Math.pow(tY - sY, 2));
    const aX = sX + (tX - sX) * (d - aSize - tSize) / d;
    const aY = sY + (tY - sY) * (d - aSize - tSize) / d;
    const vX = (tX - sX) * aSize / d;
    const vY = (tY - sY) * aSize / d;
    const lineDash = (edge.lineDash || '0').split(',');

    if (!color) {
      switch (edgeColor) {
        case 'source':
          color = source.color || defaultNodeColor;
          break;
        case 'target':
          color = target.color || defaultNodeColor;
          break;
        default:
          color = defaultEdgeColor;
          break;
      }
    }

    context.strokeStyle = color;
    context.lineWidth = size;
    context.beginPath();
    context.setLineDash(lineDash);
    context.moveTo(sX, sY);
    context.lineTo(
      aX,
      aY
    );
    context.stroke();

    context.fillStyle = color;
    context.beginPath();
    context.moveTo(aX + vX, aY + vY);
    context.lineTo(aX + vY * 0.6, aY - vX * 0.6);
    context.lineTo(aX - vY * 0.6, aY + vX * 0.6);
    context.lineTo(aX + vX, aY + vY);
    context.closePath();
    context.fill();
};

