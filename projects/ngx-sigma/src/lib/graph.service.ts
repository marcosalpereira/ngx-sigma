import { Injectable } from '@angular/core';
import { SigmaGraphService } from './sigma-graph.service';
import { GraphOptions } from './data.model';
import { FilterManagerService } from './filter-manager.service';

@Injectable()
export class GraphService {
  public sigmaInstance: any;
  public sigmaService = new SigmaGraphService(this);
  public filterManager = new FilterManagerService(this);

  public set graphOptions(options: GraphOptions) {
    this.sigmaService.graphOptions = options;
  }

}
