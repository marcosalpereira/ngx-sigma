const WORDS = {
  label: 'Nome',
  source: 'Origem',
  target: 'Destino'
};

export const translate = (s: string): string => {
  const indexed = WORDS[s];
  if (indexed) {
    return indexed;
  }
  return s.substring(0, 1).toUpperCase()
    + s.substring(1).replace(/([A-Z])/g, ' $1');
}

